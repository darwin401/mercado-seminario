import React, { PureComponent } from 'react';
import { useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Button, Card, Icon, Input } from 'react-native-elements';
import { FormatMoney } from '../../utility/formats';

export const ItemSummary = (props) => {
  const { item } = props;
  return (
    <View style={{ flexDirection: 'row' }}>
      <View style={{ width: '25%' }}>
        <Text style={{ textAlign: 'center', color: 'white' }}>{item.nombre}</Text>
      </View>
      <View style={{ width: '25%' }}>
        <Text style={{ textAlign: 'center', color: 'white' }}>{item.cantidad}</Text>
      </View>
      <View style={{ width: '30%' }}>
        <Text style={{ textAlign: 'center', color: 'white' }}>
          {FormatMoney(parseInt(item.precio))}
        </Text>
      </View>
      <View style={{ width: '20%' }}>
        <Text style={{ textAlign: 'center', color: 'white' }}>{FormatMoney(parseInt(item.precio) * parseInt(item.cantidad))}</Text>
      </View>
    </View>
  );
};
export const ItemProduct = (props) => {
  const { item, updateItems } = props;
  const [count, setCount] = useState('1');
  const addToCart = () => {
    updateItems(item, 1, parseInt(count));
  };
  return (
    <Card containerStyle={{ borderRadius: 10, backgroundColor: '#F3F3F3' }}>
      <Card.Title style={{ fontSize: 20, fontFamily: 'serif' }}>
        {item.nombre}
      </Card.Title>
      <Card.Divider />
      <Image style={styles.tinyLogo} source={{ uri: item.imagen }} />

      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontFamily: 'serif',
          margin: 2,
        }}>
        {item.nombre} {item.marca}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontFamily: 'serif',
          margin: 2,
        }}>
        {item.presentacion}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontFamily: 'serif',
          margin: 2,
        }}>
        {FormatMoney(item.precio)}
      </Text>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ width: '30%' }}>
          <Button
            onPress={() => {
              if (parseInt(count) > 1) {
                setCount(parseInt(count) - 1);
              }
            }}
            containerStyle={{ borderRadius: 50 }}
            buttonStyle={{ backgroundColor: '#F24957' }}
            icon={{
              type: 'font-awesome-5',
              name: 'minus',
              size: 15,
            }}></Button>
        </View>
        <View style={{ width: '40%' }}>
          <Input
            value={'' + count}
            inputStyle={{ textAlign: 'center' }}
            containerStyle={{ padding: 0, height: '10%' }}></Input>
        </View>
        <View style={{ width: '30%' }}>
          <Button
            onPress={() => {
              setCount(parseInt(count) + 1);
            }}
            containerStyle={{ borderRadius: 50 }}
            buttonStyle={{ backgroundColor: '#F24957' }}
            icon={{
              type: 'font-awesome-5',
              name: 'plus',
              size: 15,
            }}></Button>
        </View>
      </View>
      <Button
        containerStyle={{ marginTop: 4, borderRadius: 10 }}
        buttonStyle={{ backgroundColor: '#F24957' }}
        title="Agregar"
        onPress={addToCart}
        icon={{
          type: 'font-awesome-5',
          name: 'plus',
          size: 15,
          color: 'white',
        }}
      />
    </Card>
  );
};
export const ItemCart = (props) => {
  const { item, updateItems, removeItem } = props;
  const addToCart = () => {
    updateItems(item, 1);
  };
  return (
    <Card containerStyle={{ borderRadius: 10, backgroundColor: '#F3F3F3' }}>
      <Card.Title style={{ fontSize: 20, fontFamily: 'serif' }}>
        {item.nombre}
      </Card.Title>
      <Card.Divider />
      <Image style={styles.tinyLogo} source={{ uri: item.imagen }} />

      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontFamily: 'serif',
          margin: 2,
        }}>
        {item.nombre} {item.marca}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontFamily: 'serif',
          margin: 2,
        }}>
        <Text style={{ fontSize: 16, fontFamily: 'serif', fontWeight: 'bold' }}>
          Presentación:{' '}
        </Text>
        {item.presentacion}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontFamily: 'serif',
          margin: 2,
        }}>
        <Text style={{ fontSize: 16, fontFamily: 'serif', fontWeight: 'bold' }}>
          Valor unitario:{' '}
        </Text>
        {FormatMoney(item.precio)}
      </Text>
      <Text style={{ textAlign: 'center' }}>
        <Text style={{ fontSize: 16, fontFamily: 'serif', fontWeight: 'bold' }}>
          Cantidad:{' '}
        </Text>
        {item.cantidad}
      </Text>
      <Text style={{ textAlign: 'center' }}>
        <Text style={{ fontSize: 16, fontFamily: 'serif', fontWeight: 'bold' }}>
          Total:{' '}
        </Text>
        {FormatMoney(item.cantidad * item.precio)}
      </Text>
      <Button
        onPress={() => removeItem(item.id)}
        type="clear"
        icon={{ type: 'font-awesome-5', name: 'trash' }}></Button>
    </Card>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    marginTop: 10,
    height: 150,
    width: 150,
    alignSelf: 'center',
  },
  logo: {
    width: 66,
    height: 58,
  },
});
