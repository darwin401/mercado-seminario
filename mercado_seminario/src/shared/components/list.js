import React, { useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';
import { Button } from 'react-native-elements';
import { ItemCart, ItemProduct } from './item';
import { SearchBar } from './searchBar';


export const ListProducts = (props) => {
    const { onEndReached, onEndReachedThreshold = 0.5, initialNumToRender, updateItems, data, navigation } = props;
    const [products, setProducts] = useState([]);
    const [list, setList] = useState(data);
    const [search, setSearch] = useState("")
    const searchItems = (searchedValue) => {
        if (searchedValue === "") {
            setSearch(searchedValue);
            setList(products);
            return;
        }
        let newList = [];
        data.forEach(element => {
            if ((element.nombre && (element.nombre.toLowerCase().includes(searchedValue.toLowerCase())))
                || (element.presentacion && (element.presentacion.toLowerCase().includes(searchedValue.toLowerCase())))
            ) {
                newList.push(element);
            }
        });
        setSearch(searchedValue);
        setList(newList);
    }

    const getProducts = () => {
        fetch('http://f8ebcc0bebc8.ngrok.io/productos')
            .then((response) => response.json())
            .then((responseJson) => {
                setProducts(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });

    }
    useEffect(() => {
        getProducts();
    }, [])
    return (
        <View>
            <Button onPress={() => navigation.navigate('CreateProduct')} type='clear' icon={{ name: 'plus', type: 'font-awesome-5', color: 'white' }}></Button>
            <View style={{ marginHorizontal: 10, marginTop: 10 }}>
                <SearchBar
                    value={search}
                    onChangeText={(value) => searchItems(value)}
                />
            </View>
            <View>
                <FlatList
                    data={products}
                    renderItem={({ item }) =>
                        <ItemProduct item={item} updateItems={updateItems} />
                    }
                    keyExtractor={item => "key" + item.id}
                    onEndReached={onEndReached}
                    onEndReachedThreshold={onEndReachedThreshold}
                    initialNumToRender={initialNumToRender}
                    ListFooterComponent={<View style={{ height: 350 }} />} />
            </View>
        </View>
    )
}

export const ShoppingCart = (props) => {
    const { onEndReached, onEndReachedThreshold = 0.5, initialNumToRender, updateItems, data, removeItem } = props;
    return (
        <FlatList
            data={data}
            renderItem={({ item }) =>
                <ItemCart item={item} updateItems={updateItems} removeItem={removeItem} />
            }
            keyExtractor={item => "key" + item.id}
            onEndReached={onEndReached}
            onEndReachedThreshold={onEndReachedThreshold}
            initialNumToRender={initialNumToRender}

        />
    )
}
