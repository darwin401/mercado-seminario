import React, { PureComponent } from 'react';
import { StyleSheet, Text, View, CameraRoll,Image } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
//import { Image } from 'react-native'
//import PhotoUpload from 'react-native-photo-upload'
const PendingView = () => (
    <View
        style={{
            flex: 1,
            backgroundColor: 'lightgreen',
            justifyContent: 'center',
            alignItems: 'center',
        }}
    >
        <Text>Esperando...</Text>
    </View>
);

export class Camera extends PureComponent {
    constructor(props) {

        super(props);
        this.state = {
            flashMode: RNCamera.Constants.FlashMode.off,
            colorFlash: "white",
        }

        this.setImage = this.props.image;
        this.modalCamera = this.props.modalCamera;
    }
    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={this.state.flashMode}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                >
                    {({ camera, status }) => {
                        if (status !== 'READY') return <PendingView />;
                        return (
                            <View style={{ flex: 0, flexDirection: 'row', marginBottom: 20 }}>
                                <View style={{ width: '33%' }}>
                                    <Button containerStyle={{ alignSelf: 'flex-start' }} type="clear" icon={<Icon name="bolt" color={this.state.colorFlash} size={styles.icon.fontSize}></Icon>} buttonStyle={{ borderRadius: 20 }} onPress={this.changeFlash}></Button>
                                </View>
                                <View style={{ width: '34%' }}>
                                    <Button containerStyle={{ alignSelf: 'center' }} icon={<Icon name="camera" color="white" size={styles.icon.fontSize}></Icon>} buttonStyle={{ borderRadius: 20 }} onPress={() => this.takePicture(camera)}></Button>
                                </View>
                                <View style={{ width: '33%' }}></View>
                            </View>
                        );
                    }}
                </RNCamera>
            </View>
        );
    }

    changeFlash = () => {
        if (this.state.flashMode === RNCamera.Constants.FlashMode.on) {
            this.setState({ flashMode: RNCamera.Constants.FlashMode.off, colorFlash: 'white' });
        } else {
            this.setState({ flashMode: RNCamera.Constants.FlashMode.on, colorFlash: 'yellow' });
        }
    }

    takePicture = async function (camera) {
        const options = {
            quality: 0.5,
            base64: true,
            maxWidth: 800,
            maxHeight: 800,
        };
        const data = await camera.takePictureAsync(options);
        //  eslint-disable-next-line
        this.setImage('data:image/png;base64,'+data.base64);
        this.modalCamera(false, );
        //this.disabledInput(false);
    };
}

/*
export const UploadPhoto = () => {
    _handleButtonPress = () => {
        CameraRoll.getPhotos({
            first: 20,
            assetType: 'Photos',
        })
            .then(r => {
                this.setState({ photos: r.edges });
            })
            .catch((err) => {
                //Error Loading Images
            });
    };

    return (
        <View>
            <Button title="Load Images" onPress={this._handleButtonPress} />
            <ScrollView>
                {this.state.photos.map((p, i) => {
                    return (
                        <Image
                            key={i}
                            style={{
                                width: 300,
                                height: 100,
                            }}
                            source={{ uri: p.node.image.uri }}
                        />
                    );
                })}
            </ScrollView>
        </View>
    );

}
*/

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    icon: {
        fontSize: 30
    }
});