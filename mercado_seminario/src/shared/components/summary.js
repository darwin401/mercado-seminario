import React, {PureComponent, useEffect, useState} from 'react';
import {FlatList, Text, ToastAndroid, View} from 'react-native';
import {Button, Card} from 'react-native-elements'; 
import {FormatMoney} from '../../utility/formats';
import {ItemSummary} from './item';

export const Summary = (props) => {
  const {
    navigation,
    onEndReached,
    onEndReachedThreshold = 0.5,
    initialNumToRender,
    summary,
    closeModalSummary,
  } = props;
  const [subtotal, setSubtotal] = useState(0);
  const [iva, setIva] = useState(0);
  const [total, setTotal] = useState(0);
  useEffect(() => {
    valuesFinally();
  }, []);
  const valuesFinally = () => {
    let subtotal = 0;
    let iva = 0;
    let total = 0;
    summary.forEach((element) => {
      subtotal = subtotal + element.precio * element.cantidad;
    });
    iva = (subtotal * 19) / 100;
    total = subtotal + iva;
    setSubtotal(subtotal);
    setIva(iva);
    setTotal(total);
  };
  return (
    <Card containerStyle={{borderRadius: 10, backgroundColor: '#BF3F61'}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginVertical: 10,
        }}>
        <View style={{width: '10%'}}></View>
        <View style={{width: '70%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 20,
              fontFamily: 'serif',
              color: 'white',
              justifyContent: 'center',
            }}>
            Resumen de Compra
          </Text>
        </View>
        <View style={{width: '20%', justifyContent: 'center'}}>
          <Button
            onPress={closeModalSummary}
            type="clear"
            icon={{name: 'times', type: 'font-awesome-5', color: 'white'}}
          />
        </View>
      </View>
      <Card.Divider />
      <View style={{flexDirection: 'row'}}>
        <View style={{width: '25%'}}>
          <Text
            style={{textAlign: 'center', fontWeight: 'bold', color: 'white'}}>
            Producto
          </Text>
        </View>
        <View style={{width: '25%'}}>
          <Text
            style={{textAlign: 'center', fontWeight: 'bold', color: 'white'}}>
            Cantidad
          </Text>
        </View>
        <View style={{width: '30%'}}>
          <Text
            style={{textAlign: 'center', fontWeight: 'bold', color: 'white'}}>
            Valor unitario
          </Text>
        </View>
        <View style={{width: '20%'}}>
          <Text
            style={{textAlign: 'center', fontWeight: 'bold', color: 'white'}}>
            Total
          </Text>
        </View>
      </View>
      <FlatList
        data={summary}
        renderItem={({item}) => <ItemSummary item={item} />}
        keyExtractor={(item) => 'key' + item.id}
        onEndReached={onEndReached}
        onEndReachedThreshold={onEndReachedThreshold}
        initialNumToRender={initialNumToRender}
        ListFooterComponent={<View style={{height: 20}} />}
      />
      <Card.Divider />
      <View style={{marginTop: 10, width: '100%'}}>
        <View style={{alignSelf: 'flex-end'}}>
          <Text style={{color: 'white'}}>
            <Text style={{fontWeight: 'bold', color: 'white'}}>SUBTOTAL: </Text>
            {FormatMoney(parseInt(subtotal))}
          </Text>
          <Text style={{marginTop: 5, color: 'white'}}>
            <Text style={{fontWeight: 'bold', color: 'white'}}>IVA: </Text>
            {FormatMoney(parseInt(iva))}
          </Text>
          <Text style={{marginTop: 5, color: 'white'}}>
            <Text style={{fontWeight: 'bold', color: 'white'}}>TOTAL: </Text>
            {FormatMoney(parseInt(total))}
          </Text>
        </View>
      </View>
      <Button
        onPress={() => {
          ToastAndroid.show('Compra realizada con exito', ToastAndroid.SHORT);
          navigation.navigate('Home');
        }}
        buttonStyle={{backgroundColor: '#F24957'}}
        title="Registrar"
        containerStyle={{marginTop: 10, borderRadius: 10}}
      />
    </Card>
  );
};
