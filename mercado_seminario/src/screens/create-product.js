import React, { PureComponent } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';
import { Camera } from '../shared/components/camera/camera'
import Modal from 'react-native-modal';
import { ScrollView } from 'react-native-gesture-handler';
export class CreateProduct extends PureComponent {
    constructor() {
        super();
        this.state = {
            name: '',
            price: '',
            presentation: '',
            brand: '',
            image: '',
            modalCamera: false,
            form:{}

        }
    }

    registerProduct =  () => {
        this.setState.form = ({
            nombre: this.state.name,
            precio: this.state.price,
            presentacion: this.state.presentation,
            marca: this.state.brand,
            image: this.state.image
        })
        try {
            let config = {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(this.state.form),
            };
            let res =  fetch("http://localhost:6969/productos", config);
            let jsonRespuesta = res.json;

        } catch (error) { }
    }
    modalCamera = (flag, data) => {
        this.setState({ modalCamera: flag, });
    }
    render() {
        return (
            <>
                <ScrollView>

                </ScrollView>
                <View style={{ width: '100%', backgroundColor: '#BF3F61', height: '100%', }}>
                    <Text style={{ fontSize: 35, color: 'white', fontFamily: 'serif', textAlign: 'center', marginTop: 10 }}>Crear Producto</Text>
                    <View style={{ justifyContent: 'center', width: '100%', height: '100%', }}>
                        <ScrollView>
                            <Input
                                label='Nombre'
                                placeholderTextColor='black'
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                labelStyle={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}
                                style={[styles.input]}
                                value={this.state.name}
                                onChangeText={(e) => this.setState({ name: e })}
                                placeholder={'Nombre'}
                            >
                            </Input>
                            <Input
                                keyboardType='number-pad'
                                label='Precio'
                                placeholderTextColor='black'
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                labelStyle={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}
                                style={[styles.input]}
                                value={this.state.price}
                                onChangeText={(e) => this.setState({ price: e })}
                                placeholder={'Precio'}
                            >
                            </Input>
                            <Input
                                label='Presentación'
                                placeholderTextColor='black'
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                labelStyle={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}
                                style={[styles.input]}
                                value={this.state.presentation}
                                onChangeText={(e) => this.setState({ presentation: e })}
                                placeholder={'Presentación'}
                            >
                            </Input>
                            <Input
                                label='Marca'
                                placeholderTextColor='black'
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                labelStyle={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}
                                style={[styles.input]}
                                value={this.state.brand}
                                onChangeText={(e) => this.setState({ brand: e })}
                                placeholder={'Marca'}
                            >
                            </Input>
                            <View style={{ width: '90%', borderWidth: 2, borderColor: '#FFD700', alignSelf: 'center' }}>
                                {!this.state.image && <Button type="clear" onPress={() => this.setState({ modalCamera: true })} icon={<Icon name="camera" color='gray' type="font-awesome-5"></Icon>}></Button>}
                                {this.state.image != '' && <Image style={{ height: 350, width: "100%", alignSelf: "center" }} source={{ uri: this.state.image }} />}
                            </View>
                            <Button
                                onPress={() => {
                                    this.registerProduct();
                                }}
                                buttonStyle={{ backgroundColor: '#F24957' }}
                                title="Crear"
                                containerStyle={{ marginTop: 10, borderRadius: 10, marginHorizontal: 16 }}
                            />
                            <View style={{ height: 200 }}></View>
                        </ScrollView>

                    </View>


                </View>
                <Modal style={{ flex: 1 }}
                    isVisible={this.state.modalCamera}
                    onRequestClose={() => {
                        this.setState({
                            modalCamera: false,
                        });
                    }}
                    onBackButtonPress={() => {
                        this.setState({
                            modalCamera: false,
                        });
                    }}
                    onBackdropPress={() => {
                        this.setState({
                            modalCamera: false,
                        });
                    }}>
                    <Camera
                        modalCamera={(flag) => { this.modalCamera(flag) }}
                        image={(value) => { this.setState({ image: value }) }}
                    />
                </Modal>
            </>
        )
    }
}
const styles = StyleSheet.create({
    input: {
        fontSize: 16, backgroundColor: 'white', borderRadius: 6
    },
    containerInput: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: '90%',
        borderWidth: 1,
        marginHorizontal: 12,
        borderRadius: 10,
        borderColor: 'black',
        height: 60,
        marginTop: 3,

    },
})