import React, { PureComponent } from 'react';
import { ToastAndroid, View } from 'react-native';
import { Badge, Button, Icon, Text } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ListProducts, ShoppingCart } from '../shared/components/list';
import Modal from 'react-native-modal';
import { Summary } from '../shared/components/summary';

const data = [
  {
    id: 1,
    nombre: 'Leche Entera',
    marca: 'Colanta',
    precio: 3000,
    stock: '25',
    presentacion: 'caja',
    imagen: require('../assets/lechecaja.png'),
  },
  {
    id: 2,
    nombre: 'Aguardiente',
    marca: 'Cristal',
    precio: 31000,
    stock: '10',
    presentacion: 'Botella',
    imagen: require('../assets/aguardiente.png'),
  },
];

export class Products extends PureComponent {
  constructor() {
    super();
    this.state = {
      loading: false,
      numItem: 0,
      cartProducts: [],
      modalCart: false,
      showSummary: false,
      products: []
    };
  }
  removeItem = (id) => {
    let list = this.state.cartProducts;
    let newList = [];
    list.forEach((element) => {
      if (parseInt(element.id) !== parseInt(id)) {
        newList.push(element);
      }
    });
    if (newList.length === 0) {
      this.setState({
        modalCart: false,
        cartProducts: newList,
      });
    } else {
      this.setState({ cartProducts: newList });
    }
  };
  updateItems = (product, num, count) => {
    const list = this.state.cartProducts;
    let flag = false;
    for (let index = 0; index < list.length; index++) {
      if (parseInt(list[index].id) === parseInt(product.id)) {
        list[index].cantidad = parseInt(list[index].cantidad) + count;
        flag = true;
      }
    }
    if (!flag) {
      product.cantidad = count;
      list.push(product);
    }

    this.setState({
      numItem: this.state.numItem + num,
      cartProducts: list,
    });
  };

  componentDidMount = () => {
    this.setState({ loading: true })

  }
  render() {
    return (
      <>
        <View
          style={{ width: '100%', backgroundColor: '#BF3F61', height: '100%' }}>
          <View
            style={{
              backgroundColor: '#F24957',
              borderBottomEndRadius: 15,
              borderBottomStartRadius: 15,
              height: '10%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View style={{ width: '25%' }}></View>
            <View style={{ width: '50%' }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 28,
                  fontFamily: 'serif',
                  color: 'white',
                }}>
                Semi Market
              </Text>
            </View>
            <View style={{ width: '25%' }}>
              <TouchableOpacity
                onPress={() => {
                  if (this.state.cartProducts.length > 0) {
                    this.setState({ modalCart: true });
                  } else {
                    ToastAndroid.show(
                      'No tiene productos en el carrito',
                      ToastAndroid.SHORT,
                    );
                  }
                }}>
                <Badge
                  status="success"
                  value={this.state.cartProducts.length}
                  containerStyle={{ top: 7, right: -14, zIndex: 2 }}
                />
                <Icon
                  type="font-awesome-5"
                  name="shopping-cart"
                  color="white"
                />
              </TouchableOpacity>
            </View>
          </View>
          <ListProducts navigation={this.props.navigation} data={data} updateItems={this.updateItems} />
          <Modal
            isVisible={this.state.modalCart}
            onRequestClose={() => {
              this.setState({ modalCart: false });
            }}
            onBackButtonPress={() => {
              this.setState({ modalCart: false });
            }}
            onBackdropPress={() => {
              this.setState({ modalCart: false });
            }}>
            <ShoppingCart
              removeItem={this.removeItem}
              data={this.state.cartProducts}
              updateItems={this.updateItems}
            />
            <Button
              title="comprar"
              containerStyle={{ marginTop: 4, borderRadius: 10 }}
              buttonStyle={{ height: 60 }}
              onPress={() => this.setState({ showSummary: true })}
            />
          </Modal>
        </View>
        <Modal
          isVisible={this.state.showSummary}
          onRequestClose={() => {
            this.setState({ showSummary: false });
          }}
          onBackButtonPress={() => {
            this.setState({ showSummary: false });
          }}
          onBackdropPress={() => {
            this.setState({ showSummary: false });
          }}>
          <Summary
            navigation={this.props.navigation}
            summary={this.state.cartProducts}
            closeModalSummary={() => this.setState({ showSummary: false })}
          />
        </Modal>


      </>
    );
  }
}
