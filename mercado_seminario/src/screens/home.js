import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';

export const Home = (props) => {
    return (
        <View style={{ width: '100%', backgroundColor: '#BF3F61', height: '100%', justifyContent: 'center' }}>
            <View style={{}}>
                <Text style={{ textAlign: 'center', fontSize: 35, fontFamily: 'serif', color: 'white' }}>Semi Market</Text>
                <Image
                    style={styles.tinyLogo}
                    source={require('../assets/supermarket.png')}
                />
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 3 }}>
                    <Button
                        onPress={()=>props.navigation.navigate("Products")}
                        containerStyle={{ width: '70%', marginTop: 20 }}
                        title="Ingresar"
                    />
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
    },
    tinyLogo: {
        marginTop: 10,
        width: 250,
        height: 250,
        alignSelf: 'center'
    },
    logo: {
        width: 66,
        height: 58,
    },
});