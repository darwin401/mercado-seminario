export const NumberWithCommas = (value) => {
	if (value) {
		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	return "0"
}

export const FormatMoney = (value) => {
	return '$ ' + NumberWithCommas(value);
};



export const FormatShortDateUTC = (date, withName) => {
	var _date = date == null ? new Date() : date;
	var _withName = withName == null ? false : withName;
	if (_withName) {
		return ((((_date.getUTCDate()).toString().length == 1) ? "0" + (_date.getUTCDate()) : (_date.getUTCDate())) + " de " +
			monthsSpanish(_date.getUTCMonth() + 1) + " de " +
			_date.getUTCFullYear());
	} else {
		return (_date.getUTCFullYear()) + "-" +
			(((_date.getUTCMonth() + 1).toString().length == 1) ? "0" + (_date.getUTCMonth() + 1) : (_date.getUTCMonth() + 1)) + "-" +
			(((_date.getUTCDate()).toString().length == 1) ? "0" + (_date.getUTCDate()) : (_date.getUTCDate()));
	}


};

const monthsSpanish = (month) => {
	months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
	if (month > 0 && month < 13) {
		return months[month];
	} else {
		return month
	}
}

export const FormatDateComplete = function (una_fecha) {
	const fecha = una_fecha ? new Date(una_fecha) : new Date(Date.now());


	const miFecha = fecha.getFullYear() + "-" + (((fecha.getMonth() + 1).toString().length == 1) ? "0" + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + "-" + (((fecha.getDate()).toString().length == 1) ? "0" + (fecha.getDate()) : (fecha.getDate()));
	return miFecha;
};

export const FormatDateCompleteWithHours = function (una_fecha) {
	const fecha = una_fecha ? new Date(una_fecha) : new Date(Date.now());


	const miFecha = fecha.getFullYear() + "-" + (((fecha.getMonth() + 1).toString().length == 1) ? "0" + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + "-" + (((fecha.getDate()).toString().length == 1) ? "0" + (fecha.getDate()) : (fecha.getDate())) + " " + (fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds());
	return miFecha;
};
export const FormatOtherDateComplete = function (una_fecha) {
	const fecha = una_fecha ? new Date(una_fecha) : new Date(Date.now());


	const miFecha = (((fecha.getDate()).toString().length == 1) ? "0" + (fecha.getDate()) : (fecha.getDate())) + "/" + (((fecha.getMonth() + 1).toString().length == 1) ? "0" + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + "/" + fecha.getFullYear();


	return miFecha;
};