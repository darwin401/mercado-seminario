/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Text,

} from 'react-native';
import { Home } from './src/screens/home';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Products } from './src/screens/products';
import { CreateProduct } from './src/screens/create-product';

const Stack = createStackNavigator();
const App = () => {
  return (

    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Products" component={Products} options={{ headerShown: false }} />
        <Stack.Screen name="CreateProduct" component={CreateProduct} options={{ headerShown: false }} />
        
      </Stack.Navigator>
    </NavigationContainer>

  );
};



export default App;
